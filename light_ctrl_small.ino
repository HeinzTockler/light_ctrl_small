///http://arduino.esp8266.com/stable/package_esp8266com_index.json
/*
  На модуле установлена батарейка CR2032 вместо аккумулятора lir2032.
  Для нормальной работы модуля необходимо спаять с платы элементы, маркированные как, - R4,R5,D1 а элемент R6 заменить нулевой перемычкой.
  убрать резистор R7 рядом с кварцем.
  ПРоверить конденсаторы керамические по питанию, могут быть в районе 100пф
  Нужно это для того, что бы модуль не пытался все время подзарядить обычную одноразовую батарейку!
  Если это сделать, то модуль будет работать от этой батареи даже дольше чем от аккумулятора!

  Реле по умолчанию посажено на контакт D1, второе перевязываем на D5 и D6

  Кнопка принудительно включает свет
*/
#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <TimeLib.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>
#include <PubSubClient.h>
#include <EEPROM.h>

#ifndef STASSID
#define STASSID ""
#define STAPSK  ""
#endif

const char* ssid = STASSID;
const char* password = STAPSK;

//////////Настройки MQTT//////////////////////////////////////////////////////
const char *mqtt_server = "192.168.11.4"; // Имя сервера MQTT
const int mqtt_port = 1883; // Порт для подключения к серверу MQTT
const char *mqtt_user = "heinz"; // Логи для подключения к серверу MQTT
const char *mqtt_pass = "192422901"; // Пароль для подключения к серверу MQTT

WiFiClient wclient;
PubSubClient client(wclient);

//////////NTP//////////////////////////////////////////////////////
IPAddress timeServer(192, 168, 1, 2);
const int timeZone = 8;
const int NTP_PACKET_SIZE = 48; // NTP time is in the first 48 bytes of message
byte packetBuffer[NTP_PACKET_SIZE]; //buffer to hold incoming & outgoing packets
WiFiUDP Udp;
unsigned int localPort = 2390;      // local port to listen for UDP packets

bool debug = true;
char message[30]; //сюда кэшируем дату

//////Входа///////////////////////////////////////////////////////////////////
/*
byte relay_1 = 5; //реле основного света D1
byte relay_2 = 4; //вход для включения подсветки D2(здесь обычное реле, поэтому ставим светодиоды сюда, иначе еле горят)
byte relay_3 = 14; //реле для CO2 D5
byte relay_4 = 12; //вентилятор D7
*/
byte relay_1 = 5; //реле основного света D1
byte relay_2 = 14; //вход для включения подсветки D2(здесь обычное реле, поэтому ставим светодиоды сюда, иначе еле горят)
byte relay_3 = 4; //реле для CO2 D5
byte relay_4 = 12; //вентилятор D7

int analog=A0;

//Время работы освещения в часах
int light_time_base = 6;
int light_time_back = 2;
int light_on_base = 10;
int light_on_back = light_on_base + light_time_base;


///////Переменные/////////////////////////////////////////////////////////////
unsigned long currentTime = 0; //для организации циклов прерывания
unsigned long publicTime = 0; //для организации циклов прерывания
//unsigned long fanTime = 0; //для организации циклов прерывания
unsigned int cicle_update = 1000; //основной цикл минуты
unsigned long cicle_public = 60000; // цикл для публикации данных
unsigned long lastReconnectAttempt = 0;
//unsigned long min_cooling = 300000; //охлаждаем светильник 5 минут
//bool need_fan = false;
bool lb_on = false;
bool man_base = false;
bool man_back = false;
/*
  int curTime[2] = {
  0, 0
  };*/

void setup() {
  Serial.begin(115200);
  Serial.println("Booting");

  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);

  while (WiFi.waitForConnectResult() != WL_CONNECTED) {
    Serial.println("Connection Failed! Rebooting...");
    delay(5000);
    ESP.restart();
  }

  // Port defaults to 8266
  // ArduinoOTA.setPort(8266);

  // Hostname defaults to esp8266-[ChipID]
  ArduinoOTA.setHostname("Small_Aqua_CTRL");

  // No authentication by default
  // ArduinoOTA.setPassword("admin");

  // Password can be set with it's md5 value as well
  // MD5(admin) = 21232f297a57a5a743894a0e4a801fc3
  // ArduinoOTA.setPasswordHash("21232f297a57a5a743894a0e4a801fc3");

  ArduinoOTA.onStart([]() {
    String type;
    if (ArduinoOTA.getCommand() == U_FLASH) {
      type = "sketch";
    } else { // U_SPIFFS
      type = "filesystem";
    }

    // NOTE: if updating SPIFFS this would be the place to unmount SPIFFS using SPIFFS.end()
    Serial.println("Start updating " + type);
  });

  ArduinoOTA.onEnd([]() {
    Serial.println("\nEnd");
  });

  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
    Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
  });

  ArduinoOTA.onError([](ota_error_t error) {
    Serial.printf("Error[%u]: ", error);
    if (error == OTA_AUTH_ERROR) {
      Serial.println("Auth Failed");
    } else if (error == OTA_BEGIN_ERROR) {
      Serial.println("Begin Failed");
    } else if (error == OTA_CONNECT_ERROR) {
      Serial.println("Connect Failed");
    } else if (error == OTA_RECEIVE_ERROR) {
      Serial.println("Receive Failed");
    } else if (error == OTA_END_ERROR) {
      Serial.println("End Failed");
    }
  });

  ArduinoOTA.begin();
  Serial.println("Ready");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

  pinMode(relay_1, OUTPUT);
  pinMode(relay_2, OUTPUT);
  pinMode(relay_3, OUTPUT);
  pinMode(relay_4, OUTPUT);
  pinMode(analog, INPUT);

  client.setServer(mqtt_server, mqtt_port);
  client.setCallback(callback);

  setSyncProvider(getNtpTime);
  setSyncInterval(3600);//интервал синхронизации - 13 часов
  checkEEPROM();//смотрим че там в ПЗУ, если что обновляем

  currentTime = millis();
  publicTime = millis();

  if (debug) Serial.println("Setup_end");
  //test();
}

void loop() {
  ArduinoOTA.handle();
  set_time();
  mqqt_check();
  //основной цикл 3сек
  if (millis() > (currentTime + cicle_update)) {
    currentTime = millis();
    // manual();
    Main();
  } else if (currentTime > millis()) {
    currentTime = millis();
    Serial.println("Overflov cicle_update");
  }

  // цикл для обновления данных
  if (millis() > (publicTime + cicle_public)) {
    publicTime = millis();
    wifi_update();
    lastdata_publish(now());
    interval_base_publish();
    interval_back_publish();
  } else if (publicTime > millis()) {
    publicTime = millis();
    Serial.println("Overflov cicle_hour");
  }
}

void test() {
  digitalWrite(relay_1, false);
  digitalWrite(relay_2, false);
  digitalWrite(relay_3, false);
  digitalWrite(relay_4, false);
  delay(2000);
  digitalWrite(relay_1, true);
  delay(2000);
  digitalWrite(relay_2, true);
  delay(2000);
  digitalWrite(relay_3, true);
  delay(2000);
  digitalWrite(relay_4, true);
  delay(2000);
  digitalWrite(relay_1, false);
  digitalWrite(relay_2, false);
  digitalWrite(relay_3, false);
  digitalWrite(relay_4, false);
}
