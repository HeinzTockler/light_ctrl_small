void Main() {
  //if (debug)Serial.println("main");
  //getTime();
  int tmp = analogRead(analog);
  //if (debug)Serial.print("analog=");
 // if (debug)Serial.println(tmp);
  bool man_base = false;
  bool man_back = false;
  if ((tmp < 512) && (tmp > 100))man_base = true;
  if (tmp > 900)man_back = true;

  if (man_base) {
    digitalWrite(relay_1, false);
    if (debug)Serial.println("man_base");
  } else {
    //в зависимости от времени включаем необходимые выхода
    if (hour() >= light_on_base && hour() < (light_on_base + light_time_base) ) {
      digitalWrite(relay_1, false);
      //включаем обдув светильника
      digitalWrite(relay_3, false);
      //включаем подачу углекислоты
      digitalWrite(relay_4, false);
    }

    //в конце периода света выключаем все
    if (hour() >= light_on_base + light_time_base) {
      digitalWrite(relay_1, true);
      digitalWrite(relay_3, true);
      digitalWrite(relay_4, true);

    }

    if (hour() < light_on_base) {
      digitalWrite(relay_1, true);
      digitalWrite(relay_3, true);
      digitalWrite(relay_4, true);
    }
  }

  //если есть команда на ручное включение то включаем
  if (man_back) {
    digitalWrite(relay_2, true);
    if (debug)Serial.println("man_back");
  } else {
    if (hour() >= light_on_back && hour() < light_on_back + light_time_back ) {
      digitalWrite(relay_2, true);
    }

    //в конце периода или перед его началом выключаем все
    if (hour() >= light_on_back + light_time_back)
      digitalWrite(relay_2, false);

    if (hour() < light_on_back)
      digitalWrite(relay_2, false);
  }
}

/*
  void digitalClockDisplay()
  {
  // digital clock display of the time
  Serial.print(hour());
  printDigits(minute());
  printDigits(second());
  Serial.print(' ');
  Serial.print(day());
  Serial.print(' ');
  Serial.print(month());
  Serial.print(' ');
  Serial.print(year());
  Serial.println();
  }

  void printDigits(int digits)
  {
  // utility function for digital clock display: prints preceding colon and leading 0
  Serial.print(':');
  if (digits < 10)
    Serial.print('0');
  Serial.print(digits);
  }*/

/*
  void manual() {
  man_back = digitalRead(man_1);
  //if (debug)Serial.print("man_back= ");
  //if (debug)Serial.println(man_back);
  man_base = digitalRead(man_2);
  // if (debug)Serial.print("man_base= ");
  // if (debug)Serial.println(man_base);
  }*/

void set_time() {
  //2020,11,12,15,16,00
  // check for input to set the RTC, minimum length is 12, i.e. yy,m,d,h,m,s
  if (Serial.available() >= 12) {
    static time_t tLast;
    time_t t;
    tmElements_t tm;
    // note that the tmElements_t Year member is an offset from 1970,
    // but the RTC wants the last two digits of the calendar year.
    // use the convenience macros from the Time Library to do the conversions.
    int y = Serial.parseInt();
    if (y >= 100 && y < 1000) {}
    // Serial << F("Error: Year must be two digits or four digits!") << endl;
    else {
      if (y >= 1000)
        tm.Year = CalendarYrToTm(y);
      else    // (y < 100)
        tm.Year = y2kYearToTm(y);
      tm.Month = Serial.parseInt();
      tm.Day = Serial.parseInt();
      tm.Hour = Serial.parseInt();
      tm.Minute = Serial.parseInt();
      tm.Second = Serial.parseInt();
      t = makeTime(tm);
      // RTC.set(t);        // use the time_t value to ensure correct weekday is set
      setTime(t);
      // Serial << F("RTC set to: ");
      // digitalClockDisplay();
      //  Serial << endl;
      // dump any extraneous input
      while (Serial.available() > 0) Serial.read();
    }
  }
}
